//
//  TableViewCell(Itens).swift
//  Pokedex.Rezende
//
//  Created by COTEMIG on 31/03/22.
//

import UIKit

class TableViewCell_Itens_: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

 
   
    
  
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var img_pokemon: UIImageView!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var conteinerTipo1: UIView!
    @IBOutlet weak var tipo1: UILabel!
    @IBOutlet weak var containerTipo2: UIView!
    @IBOutlet weak var tipo2: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
