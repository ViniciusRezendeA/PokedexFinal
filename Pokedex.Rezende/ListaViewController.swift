//
//  ListaViewController.swift
//  Pokedex.Rezende
//
//  Created by COTEMIG on 07/04/22.
//

import UIKit
extension ListaViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "pokemonEspecifico", sender: indexPath.row)
    }
   
}
extension UIColor {

static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)->UIColor{
return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)

}

static func colorA() -> UIColor {
    return UIColor(red: 72/255, green: 208/255, blue: 176/255, alpha: 1)
    }


static func colorB() -> UIColor {
return UIColor(red: 173/255, green: 104/255, blue: 173/255, alpha: 1)
}
}
class ListaViewController: UIViewController, UITableViewDataSource {
    
    struct pokemon {
        var nome :String
        var id: String
        var tipo :String
        var tipo2:String
        var foto_url: String

    }

    var listaPokemon = [pokemon]()
    
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.iniciarPokemons()
        self.table.dataSource = self
        self.table.delegate = self
    }
    
    
  
    func iniciarPokemons() {
        self.listaPokemon = [
            pokemon(nome: "Bulbassauro", id: "001",tipo:"grama",tipo2: "veneno", foto_url:"bulbassauro"),
            pokemon(nome: "Ivyssauro", id: "002",tipo:"grama",tipo2: "veneno",  foto_url:"ivysaur"),
            pokemon(nome: "Venussauro", id: "003",tipo:"grama",tipo2: "veneno",  foto_url:"venusaur"),
            pokemon(nome: "Charmander", id: "004",tipo:"fogo",tipo2: "",  foto_url:"charmander"),
            pokemon(nome: "Charmeleon", id: "005",tipo:"fogo",tipo2: "",  foto_url:"charmeleon"),
            pokemon(nome: "Charizard", id: "006",tipo:"fogo",tipo2: "voador",  foto_url:"charizard"),
            pokemon(nome: "Squirtle", id: "007",tipo:"agua",tipo2: "",  foto_url:"squirtle"),
            pokemon(nome: "Wartortle", id: "008",tipo:"agua",tipo2: "",  foto_url:"wartortle"),
            pokemon(nome: "Blastoise", id: "009",tipo:"agua",tipo2: "",  foto_url:"blastoise"),
            pokemon(nome: "Pikachu", id: "010",tipo:"eletrico",tipo2: "",  foto_url:"pikachu"),
        ]
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "pokemon", for: indexPath) as? TableViewCell_Itens_{
            cell.nome.text = self.listaPokemon[indexPath.row].nome
            cell.tipo1.text = self.listaPokemon[indexPath.row].tipo
            cell.tipo2.text = self.listaPokemon[indexPath.row].tipo2
            cell.id.text = self.listaPokemon[indexPath.row].id
            cell.img_pokemon.image = UIImage(named:self.listaPokemon[indexPath.row].foto_url)
            if  self.listaPokemon[indexPath.row].tipo2 == ""{
                cell.containerTipo2.isHidden = true
            }else{
                cell.containerTipo2.isHidden = false
            }
            if self.listaPokemon[indexPath.row].tipo == "grama" {
                let grama = UIColor.rgb(red: 72, green: 208, blue: 176, alpha: 1)
                cell.conteinerTipo1.backgroundColor = grama
                cell.containerTipo2.backgroundColor = grama
                cell.container.backgroundColor = grama
                
            }else if self.listaPokemon[indexPath.row].tipo == "fogo"{
                let fogo = UIColor.rgb(red: 251, green: 108, blue: 108, alpha: 1)
                cell.conteinerTipo1.backgroundColor = fogo
                cell.containerTipo2.backgroundColor = fogo
                cell.container.backgroundColor = fogo
            }else if self.listaPokemon[indexPath.row].tipo == "eletrico"{
               
                let eletrico = UIColor.rgb(red: 255, green: 206, blue: 75, alpha: 1)
                cell.conteinerTipo1.backgroundColor = eletrico
                cell.containerTipo2.backgroundColor = eletrico
                cell.container.backgroundColor = eletrico
            }else if self.listaPokemon[indexPath.row].tipo == "agua"{
                let agua = UIColor.rgb(red: 119, green: 189, blue: 254, alpha: 1)
                cell.conteinerTipo1.backgroundColor = agua
                cell.containerTipo2.backgroundColor = agua
                cell.container.backgroundColor = agua
            }
              
                
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaPokemon.count
    }
   
    
   
    
 
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

